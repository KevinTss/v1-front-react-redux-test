export const padList = (state = null, action) => {
  switch (action.type) {
    case "FETCH_TEXTS": {
      const newObject = {
        padName: `${action.padName}`,
        navList: {
          h1: "",
          h2: []
        },
        text: action.payload
      };
      const copiedState = state.slice(0);
      copiedState.push(newObject);
      return copiedState;
    }
    case "FILLED_LIST": {
      const copiedState = state.slice(0);
      copiedState[copiedState.length - 1].navList = {
        h1: action.mainTitle,
        h2: action.subTilesArray
      };

      return copiedState;
    }
    default: {
      return state;
    }
  }
};

export const orderList = (state = null, action) => {
  switch (action.type) {
    case "CHANGE_ORDER": {
      return action.newOrder;
    }
    default: {
      return state;
    }
  }
};
