import { combineReducers } from 'redux';
import { padList, orderList } from './fetch-texts-reducer';

const rootReducer = combineReducers({
    padList,
    orderList
});

export default rootReducer;
