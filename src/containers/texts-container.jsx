import React, { Component } from "react";
import { connect } from "react-redux";
import TextComponent from "../components/text-component";
import NavBar from "../components/navbar";
import { fetchTexts, filledList, changeOrder } from "../actions/index";

class TextsContainer extends Component {
  state = {
    padLinkArray: []
  };

  componentDidUpdate(prevProps) {
    // if (this.props.padList[this.props.padList.length - 1].navList.h1 !== prevProps.padList[prevProps.padList.length - 1].navList.h1) {
    if (
      prevProps.padList.length > 0 &&
      this.props.padList[this.props.padList.length - 1].navList.h1 !==
        prevProps.padList[prevProps.padList.length - 1].navList.h1
    ) {
      const pad = document.querySelector(
        `#pad${this.props.padList.length - 1}`
      );
      const mainTitle = pad.querySelector("h1").innerHTML;
      let subTilesArray = Array.from(pad.querySelectorAll("h2"));
      subTilesArray = subTilesArray.map(subTitle => {
        return subTitle.innerHTML;
      });
      this.props.dispatch(filledList(mainTitle, subTilesArray));
    }
  }

  componentDidMount() {
    const padLinksArray = [
      "annuel.framapad.org/p/intro",
      "annuel.framapad.org/p/engineering-care"
    ];
    padLinksArray.forEach((pad, id) => {
      this.props.dispatch(fetchTexts(pad, id));
    });
  }

  handleSubmit = e => {
    e.preventDefault();
    let inputValue = e.currentTarget.querySelector("input").value;
    inputValue = inputValue.replace(/\s+/, "");
    const padLinkArray = inputValue.split("https://");
    this.setState({ padLinkArray });
    setInterval(() => {
      padLinkArray.forEach(padLink => {
        const padName = padLink.replace(/\s+|^.+(\/p\/)/, "");
        if (padLink !== "") {
          this.props.dispatch(fetchTexts(padLink, padName));
        }
      });
    }, 2000);
  };

  render() {
    return (
      <div id="app">
        <div className="big-grid">
          <form onSubmit={this.handleSubmit}>
            <input type="text" />
          </form>
          <NavBar />
          <section className="chapitre-container">
            {this.props.padList &&
              this.props.padList.map((padObj, i) => {
                return <TextComponent padText={padObj.text} id={i} key={i} />;
              })}
          </section>
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    padList: state.padList,
    orderList: state.orderList
  };
}

export default connect(mapStateToProps)(TextsContainer);
