export const changeOrder = (newOrder) => {
    return {
        type: 'CHANGE_ORDER',
        newOrder
    }
}



export const fetchTexts = (padLink, padName) => {
    return fetch(`https://${padLink}/export/txt`)
        .then(response => response.text())
        .then(data => {
            return {
                type: 'FETCH_TEXTS',
                padName,
                payload: data
            }
        })

}


export const filledList = (mainTitle, subTilesArray) => {
    return {
        type: 'FILLED_LIST',
        mainTitle,
        subTilesArray
    }
}
