import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, combineReducers, applyMiddleware } from 'redux';
import reduxPromise from 'redux-promise';
import logger from 'redux-logger';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { createHistory as history } from 'history';
import rootReducer from './reducers/index'
import TextsContainer from './containers/texts-container'
import data from "./data/data.json"

import '../assets/stylesheets/application.scss';



const initialState = data;


const middlewares = applyMiddleware(reduxPromise, logger);

// render an instance of the component in the DOM
ReactDOM.render(
  <Provider store={createStore(rootReducer, initialState, middlewares)}>
    <Router history={history}>
      <Switch>
        <Route path="/" component={TextsContainer}>
          {/* <IndexRoute component={PhotoGrid}></IndexRoute>
          <Route path="/view/:postId" component={Single}></Route> */}
        </Route>
      </Switch>
    </Router>
  </Provider>,
  document.getElementById('root')
);
