import React, { Component } from 'react';
import ReactMarkdown from 'react-markdown'


class TextComponent extends Component {

    render() {
        const markdown = this.props.padText;
        const id = `pad${this.props.id}`
        return (
            <div className="text-component" id={id}>
                {markdown &&
                    <ReactMarkdown source={markdown} />
                }
            </div >
        );
    }
}

export default TextComponent
