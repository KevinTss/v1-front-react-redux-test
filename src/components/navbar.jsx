import React, { Component } from 'react';
import { connect } from 'react-redux';
import { changeOrder } from '../actions/index';

class NavBar extends Component {
    state = {
        items: [],
        draggedItem: ""
    };

    componentDidUpdate(prevProps) {
        if (prevProps.padList && prevProps.padList.pop() !== this.props.padList.pop()) {
            console.log("update2")
            this.props.padList.forEach((padObj,i) => {
                if (padObj.navList.h1 && padObj.navList.h1){
                    const joined = this.state.items.concat(padObj.navList.h1);
                    this.setState({ items: joined })
                    console.log(this.state.items)
                }
            });
        }
    }

    showSubList = (e) => {
    }

    onDragStart = (e, index) => {
        this.state.draggedItem = this.state.items[index];
        e.dataTransfer.effectAllowed = "move";
        e.dataTransfer.setData("text/html", e.target.parentNode);
        e.dataTransfer.setDragImage(e.target.parentNode, 20, 20);
    }

    onDragOver = index => {
        const draggedOverItem = this.state.items[index];
        // if the item is dragged over itself, ignore
        if (this.state.draggedItem === draggedOverItem) {
            return;
        }
        // filter out the currently dragged item
        let items = this.state.items.filter(item => item !== this.state.draggedItem);
        // add the dragged item after the dragged over item
        items.splice(index, 0, this.state.draggedItem);
        this.setState({ items });
    }

    onDragEnd = () => {
        this.props.dispatch(changeOrder(this.state.items))
    };

    render() {
        return (
            <nav id="nav">
                <ul className="chapitre-list" onClick={this.showSubList}>
                    {this.state.items[0] &&
                        this.state.items.map((pad, i) => {
                            return (
                                <li key={i} onDragOver={() => this.onDragOver(i)}>
                                    <div
                                        className="drag"
                                        draggable
                                        onDragStart={e => this.onDragStart(e, i)}
                                        onDragEnd={this.onDragEnd}
                                    >
                                    </div>
                                    <span className="content">{pad}</span>
                                </li>
                            )
                        })}
                </ul>
            </nav >
        );
    }
}

function mapStateToProps(state) {
    return {
        padList: state.padList
    }
}

export default connect(mapStateToProps)(NavBar)

